import React from "react";
import { useStateValue } from "./StateProvider";
import "./CheckOut.css";
import CheckOutProduct from "./CheckOutProduct";
import Subtotal from "./Subtotal";

function CheckOut() {
  const [{ basket }] = useStateValue();

  return (
    <div className="checkout">
      <div className="checkout__left">
        <img
          className="checkout__ad"
          src="https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_.jpg"
        />
        {basket?.length === 0 ? (
          <h2>Your shopping basket is empty</h2>
        ) : (
          <div className="checkout__title">
            <h2>Your shopping basket</h2>
            {basket?.map((item) => {
              return (
                <CheckOutProduct
                  id={item.id}
                  title={item.title}
                  image={item.image}
                  price={item.price}
                  rating={item.rating}
                />
              );
            })}
          </div>
        )}
      </div>
      {basket.length > 0 && (
        <div className="checkout__right">
          <Subtotal />
        </div>
      )}
    </div>
  );
}

export default CheckOut;
