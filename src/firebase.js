import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDhowvh8qfHu3GB9GML3O3wlIgHfiQEWwo",
  authDomain: "clone-c09b0.firebaseapp.com",
  databaseURL: "https://clone-c09b0.firebaseio.com",
  projectId: "clone-c09b0",
  storageBucket: "clone-c09b0.appspot.com",
  messagingSenderId: "447668017314",
  appId: "1:447668017314:web:47feff4f3dcd936cb0dffb",
  measurementId: "G-FZHPN8WDZG",
});

const db = firebaseApp.firestore();
const auth = firebase.auth();
export { auth };
