import React from "react";
import "./Home.css";
import Product from "./Product";

function Home() {
  return (
    <div className="home">
      <img
        className="home__image"
        src="https://scontent.fktm8-1.fna.fbcdn.net/v/t1.15752-9/109115795_2605708553036577_3683202744124907529_n.jpg?_nc_cat=110&_nc_sid=b96e70&_nc_ohc=Hzn2gbkN8cwAX-2u6lu&_nc_ht=scontent.fktm8-1.fna&oh=07e96364d068c52ce9a95724c8af8536&oe=5F331B1D"
      />
      {/* product id,title,price,rating,image */}

      <div className="home__row">
        <Product
          id="12345"
          title="the lost girl"
          price={400}
          rating={5}
          image="https://images.unsplash.com/photo-1543512214-bb1d7ac7e925?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
        />
        <Product
          id="12345"
          title="the lost girl"
          price={400}
          rating={5}
          image="https://images.unsplash.com/photo-1560343090-f0409e92791a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
        />
      </div>
      <div className="home__row">
        <Product
          id="12345"
          title="the lost girl"
          price={400}
          rating={5}
          image="https://images.unsplash.com/photo-1526434426615-1abe81efcb0b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
        />
        <Product
          id="12345"
          title="the lost girl"
          price={400}
          rating={5}
          image="https://images.unsplash.com/photo-1491637639811-60e2756cc1c7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
        />
        <Product
          id="12345"
          title="the lost girl"
          price={400}
          rating={5}
          image="https://scontent.fktm8-1.fna.fbcdn.net/v/t1.15752-9/107298065_2638855526329866_2369879082591393563_n.jpg?_nc_cat=103&_nc_sid=b96e70&_nc_ohc=7uIITjIO65YAX_jE7i8&_nc_ht=scontent.fktm8-1.fna&oh=b374a230ce7bb92469b9b33be097cb5a&oe=5F32B761"
        />
      </div>
      <div className="home__row">
        <Product
          id="12345"
          title="the lost girl"
          price={400}
          rating={5}
          image="https://scontent.fktm8-1.fna.fbcdn.net/v/t1.15752-9/107812239_2753641058254941_6736581160275348881_n.jpg?_nc_cat=100&_nc_sid=b96e70&_nc_ohc=SXPt0B9VG2IAX8iqUKH&_nc_ht=scontent.fktm8-1.fna&oh=29f7c1233696ea21eb9a44d5a22be42b&oe=5F34ED4F"
        />
      </div>
    </div>
  );
}

export default Home;
